import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import three from 'three';

import './main.html';

var container;

var camera, scene, renderer;
var cameraCube, sceneCube;

var mesh, lightMesh, geometry;
var spheres = [];

var directionalLight, pointLight;

var mouseX = 0, mouseY = 0;

var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

Template.world.onCreated(function() {

  WebFont.load({
    google: {
      families: ['Roboto']
    },
    active: function() {
      // Font's have loaded.. Do something!
    }
  });
});

Template.world.onRendered(function helloOnCreated() {
  // counter starts at 0
  THREE = three;
  /**
   * @author alteredq / http://alteredqualia.com/
   * @author mr.doob / http://mrdoob.com/
   */

  var Detector = {

   canvas: !! window.CanvasRenderingContext2D,
   webgl: ( function () {

     try {

       var canvas = document.createElement( 'canvas' ); return !! ( window.WebGLRenderingContext && ( canvas.getContext( 'webgl' ) || canvas.getContext( 'experimental-webgl' ) ) );

     } catch ( e ) {

       return false;

     }

   } )(),
   workers: !! window.Worker,
   fileapi: window.File && window.FileReader && window.FileList && window.Blob,

   getWebGLErrorMessage: function () {

     var element = document.createElement( 'div' );
     element.id = 'webgl-error-message';
     element.style.fontFamily = 'monospace';
     element.style.fontSize = '13px';
     element.style.fontWeight = 'normal';
     element.style.textAlign = 'center';
     element.style.background = '#fff';
     element.style.color = '#000';
     element.style.padding = '1.5em';
     element.style.width = '400px';
     element.style.margin = '5em auto 0';

     if ( ! this.webgl ) {

       element.innerHTML = window.WebGLRenderingContext ? [
         'Your graphics card does not seem to support <a href="http://khronos.org/webgl/wiki/Getting_a_WebGL_Implementation" style="color:#000">WebGL</a>.<br />',
         'Find out how to get it <a href="http://get.webgl.org/" style="color:#000">here</a>.'
       ].join( '\n' ) : [
         'Your browser does not seem to support <a href="http://khronos.org/webgl/wiki/Getting_a_WebGL_Implementation" style="color:#000">WebGL</a>.<br/>',
         'Find out how to get it <a href="http://get.webgl.org/" style="color:#000">here</a>.'
       ].join( '\n' );

     }

     return element;

   },

   addGetWebGLMessage: function ( parameters ) {

     var parent, id, element;

     parameters = parameters || {};

     parent = parameters.parent !== undefined ? parameters.parent : document.body;
     id = parameters.id !== undefined ? parameters.id : 'oldie';

     element = Detector.getWebGLErrorMessage();
     element.id = id;

     parent.appendChild( element );

   }

  };

  // browserify support
  if ( typeof module === 'object' ) {

   module.exports = Detector;

  }


    if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

    document.addEventListener( 'mousemove', onDocumentMouseMove, false );

    init();
    animate();


});

function onWindowResize() {

  windowHalfX = window.innerWidth / 2,
  windowHalfY = window.innerHeight / 2,

  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  cameraCube.aspect = window.innerWidth / window.innerHeight;
  cameraCube.updateProjectionMatrix();

  renderer.setSize( window.innerWidth, window.innerHeight );

}

function onDocumentMouseMove(event) {

  mouseX = ( event.clientX - windowHalfX ) * 10;
  mouseY = ( event.clientY - windowHalfY ) * 10;

}

//

function animate() {

  requestAnimationFrame( animate );

  render();

}

function render() {

  var timer = 0.0001 * Date.now();

  for ( var i = 0, il = spheres.length; i < il; i ++ ) {

    var sphere = spheres[ i ];

    sphere.position.x = 5000 * Math.cos( timer + i );
    sphere.position.y = 5000 * Math.sin( timer + i * 1.1 );

  }

  camera.position.x += ( mouseX - camera.position.x ) * .05;
  camera.position.y += ( - mouseY - camera.position.y ) * .05;

  camera.lookAt( scene.position );
  cameraCube.rotation.copy( camera.rotation );

  renderer.render( sceneCube, cameraCube );
  renderer.render( scene, camera );

}

function init() {

  container = document.createElement( 'div' );
  document.body.appendChild( container );

  camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 100000 );
  camera.position.z = 3200;

  cameraCube = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 100000 );

  scene = new THREE.Scene();
  sceneCube = new THREE.Scene();

  var geometry = new THREE.SphereBufferGeometry( 100, 32, 16 );

  var path = "textures/cube/skybox/";
  var format = '.jpg';
  var urls = [
    path + 'px' + format, path + 'nx' + format,
    path + 'py' + format, path + 'ny' + format,
    path + 'pz' + format, path + 'nz' + format
  ];

  var textureCube = new THREE.CubeTextureLoader().load( urls );
  textureCube.mapping = THREE.CubeRefractionMapping;

  var material = new THREE.MeshBasicMaterial( { color: 0xffffff, envMap: textureCube, refractionRatio: 0.95 } );

  for ( var i = 0; i < 100; i ++ ) {

    var mesh = new THREE.Mesh( geometry, material );

    mesh.position.x = Math.random() * 10000 - 5000;
    mesh.position.y = Math.random() * 10000 - 5000;
    mesh.position.z = Math.random() * 10000 - 5000;

    mesh.scale.x = mesh.scale.y = mesh.scale.z = Math.random() * 6 + 1;

    scene.add( mesh );

    spheres.push( mesh );

  }

  // Skybox

  var shader = THREE.ShaderLib[ "cube" ];
  shader.uniforms[ "tCube" ].value = textureCube;

  var material = new THREE.ShaderMaterial( {

    fragmentShader: shader.fragmentShader,
    vertexShader: shader.vertexShader,
    uniforms: shader.uniforms,
    depthWrite: false,
    side: THREE.BackSide

  } ),

  mesh = new THREE.Mesh( new THREE.BoxGeometry( 100, 100, 100 ), material );
  sceneCube.add( mesh );

  //

  renderer = new THREE.WebGLRenderer();
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  renderer.autoClear = false;
  container.appendChild( renderer.domElement );

  //

  window.addEventListener( 'resize', onWindowResize, false );

}
