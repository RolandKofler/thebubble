Blockheight = new ReactiveVar(web3.eth.blockNumber);

Meteor.setInterval(function() {
        Blockheight.set(web3.eth.blockNumber);
}, 5000 /*1 minute poll*/);

Template.menu.helpers({
  blockhight(){
    return EthBlocks.latest.number;//web3.eth.coinbase;
  },
  web3version(){
    return web3.version.api;
  }
})
