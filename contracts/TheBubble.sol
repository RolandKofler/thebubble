contract TheBubble {

    event Log_RaceCreated();
    event Log_BubbleCreated(uint when, address who, bytes32 which);
    event Log_BubbleInflatedByBlock();
    event Log_BubbleInflatedByUser(bytes32 which, uint howBig);//TODO: indexed?
    event Log_BubbleBursted(bytes32 which);
    event Log_RaceEnded();

    address constant private creator = msg.sender;
    function getCreator() returns (address) {return creator;} // necessary workaround because pudding is not generating me a getter

    uint256 constant FEE = 0.1 ether;
    uint256 constant start_block = block.number;
    uint private raceDuration=185142; //about a month.
    function getRaceDuration() external constant returns(uint){return raceDuration;}

    uint256 constant last_block=start_block + raceDuration;
    uint256 constant SELF_INFLATION_REWARD = 1000;
    uint256 finalSumAllBubbleSizes=0;

    struct BubbleData {
        uint sizeByInflation;
        uint size;
        uint blockCreated;
        address owner;
    }

    struct Data {
        mapping (bytes32 => BubbleData) bubbles;
        bytes32[] activeBubbleList;
    }

    Data bubbleMap;

    /**  if @param _notOver true then this throws exception if race is over, and if false then it throws if race not over*/
    modifier ifRaceNotOver(bool _notOver){
        if (isNotOver(_notOver)) throw;
        _
    }

    function isNotOver(bool _notOver) public constant returns(bool){
        return (last_block < block.number == _notOver);
    }


    function TheBubble(uint _raceDuration) public{
        raceDuration=_raceDuration;
        Log_RaceCreated();
    }

    // Collection functions

    function contains(bytes32 key) public returns (bool){
        return bubbleMap.bubbles[key].owner > 0x0; //TODO: make sure this is a good way to test an address
    }

    function get(bytes32 key) internal returns (BubbleData){
        if (!contains(key)) throw;
        return bubbleMap.bubbles[key];
    }

    function remove(bytes32 key) internal {
        delete bubbleMap.bubbles[key];
        var len = bubbleMap.activeBubbleList.length;
        for( var i = 0; i < len; i ++){
            if (bubbleMap.activeBubbleList[i] == key)
                delete bubbleMap.activeBubbleList[i]; //TODO: is having a gap costly?
        }

    }

    // End Collection functions

    /*Create a new bubble for Player */
    function createBubble(bytes32 key) ifRaceNotOver(true) external returns (bool){
        //if (msg.value != fee) throw;
        if (contains(key)) throw;

        bubbleMap.activeBubbleList.push(key);
        var b = BubbleData({sizeByInflation:0, size:0, blockCreated:block.number, owner: msg.sender });
        bubbleMap.bubbles[key]= b;
        Log_BubbleCreated(b.blockCreated, b.owner, key);
        return true;
    }

    function inflateBubble(bytes32 key, uint8 howMuch) ifRaceNotOver(true) {
        //NOTNEEDED: if (!contains(passphrase)) throw;
        var bubble = get(key);
        //if (msg.value != fee) throw;
        if(bubble.owner != msg.sender) throw;
        if(howMuch > 99) throw;
        if(howMuch < 1) throw;

        uint8 burstPoint= randomGen(0); //TODO: totaly unsecure

        bool hasBursted = howMuch > burstPoint;

        if(hasBursted){
          remove(key);
          Log_BubbleBursted(key);
        }

        if(!hasBursted){
          bubble.sizeByInflation=+howMuch;
          Log_BubbleInflatedByUser(key, bubble.sizeByInflation);
        }
    }

    function prepareCashout() ifRaceNotOver(false) external{
        if (msg.sender != creator) throw;
        //TODO: make sure this never can exceed the Block Gas Limit
        uint sum=0;
        for (uint256 i= 0; i < bubbleMap.activeBubbleList.length; i++){
            var key = bubbleMap.activeBubbleList[i];
            if (!contains(key))
                continue;
            var bubble = get(key);
            uint size= bubble.sizeByInflation;
            uint trueSize = getTrueSize(key);
            bubble.size = size;
            sum =+ trueSize;
        }
        finalSumAllBubbleSizes = sum;

    }


    /// Get your fair share
    function withdraw(bytes32 passphrase) ifRaceNotOver(false) external{
        if (finalSumAllBubbleSizes==0) throw; // I've not preaperd the cashout yet
        var mySize =getTrueSize(passphrase);
        var share = (mySize * this.balance)/finalSumAllBubbleSizes; //TODO: better reward for top players, like quadratic
        // It is important to set this to zero because the recipient
        // can call this function again as part of the receiving call
        // before `send` returns.
        //pendingReturns[msg.sender] = 0;

        if (!msg.sender.send(share))
            throw; // If anything fails, this will revert the changes above
    }

    function getTrueSize(bytes32 key) public constant returns (uint bubbleSize){
        var bubble = get(key);
        return getTrueSize(bubble.sizeByInflation, bubble.blockCreated);
    }

    function getTrueSize(uint sizeByInflation, uint blockCreated) internal constant returns (uint bubbleSize){
        var maxBlock = block.number > last_block ? last_block : block.number;
        return sizeByInflation * SELF_INFLATION_REWARD + (maxBlock - blockCreated);
    }

    /*TODO:  Unsecure Generates a random number from 0 to 100 based on the last block hash */
    function randomGen(uint seed) private constant returns (uint8 randomNumber) {
        return(uint8(sha3(block.blockhash(block.number-1), seed ))%100);
    }

    function () {
        // This function gets executed if an transaction with invalid data is sent to
        // the contract or just ether without data. We revert the send so that no-one
        // accidentally loses money when using the contract.
        throw;
    }
}
