keccak_256 = require('js-sha3').keccak_256;
var bubbleHash= keccak_256('my secret passphrase 1');

contract('TheBubble', function(accounts) {
  it("should have a creator", function(done) {
    var theBubble = TheBubble.deployed();
    theBubble.getCreator({from:accounts[0]}).then(function(creator) {
      assert.ok(creator);
    }).then(done).catch(done);
  });
  it("should create a bubble", function(done){
    var theBubble = TheBubble.deployed();
    var theBubbleCoinEthBalance;

    theBubble.createBubble(bubbleHash, {from:accounts[0]}).then(function(result){
      assert.ok(result)
    }).then(done).catch(done);
  });
  it("should inflate the bubble", function(done) {
    var theBubble = TheBubble.deployed();

    theBubble.inflateBubble(bubbleHash, 1, {from:accounts[0]}).then(function(result) {
      assert.ok(result);
    }).then(done).catch(done);
  });
  it("should burst the bubble", function(done) {
    var theBubble = TheBubble.deployed();
    theBubble.inflateBubble(bubbleHash, 99, {from:accounts[0]}).then(function(result)
    {
      assert.ok(result);
    }).then(done).catch(done);
  });
/*  it("race should be over", function(done) {
    var theBubble = TheBubble.deployed();
    theBubble.isNotOver(false, {from:accounts[0]}).then(function(result) {
      assert.ok(result);
    }).then(done).catch(done);
  });*/
  it("should burst the bubble", function(done) {
    var theBubble = TheBubble.deployed();
    theBubble.inflateBubble(bubbleHash, 99, {from:accounts[0]}).then(function(result)
    {
      assert.ok(result);
    }).then(done).catch(done);
  });
});
