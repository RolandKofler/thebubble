module.exports = {
  build: {
    "app.html": {
      "files":["client/main.html"],
      "post-process": []
    },
    "app.js": {
      "files":["client/main.js"],
      "post-process": [
        "bootstrap",
        "frontend-dependencies"
      ]
    },
    "app.css": [
      "client/main.css"
    ]

  },
  deploy: [
    //"TheBubble",
    "TheError"
  ],
  rpc: {
    host: "localhost",
    port: 8545
  }
};
